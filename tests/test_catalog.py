# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from tests.auth import librarian, user

def test_listing_catalog_for_book(client):
    response = client.get('/catalog/get_for_book?id_book=8')
    assert response.status_code == 200
    catalog = response.get_json()
    assert catalog[0]["shelf_mark"] == 'C06'
    assert catalog[1]["shelf_mark"] == 'C10'

def test_invalid_catalog_id(client):
    response = client.get('/catalog/get_for_book?id_book=alsiefj')
    assert response.status_code == 400

def test_reserve(client):
    response = client.post('/catalog/2/reserve', auth=user)
    assert response.status_code == 204