# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from hashlib import sha256
import os
from flask import Blueprint, jsonify, request

from flask_httpauth import HTTPBasicAuth
import click

from erudite.db import get_db

auth = HTTPBasicAuth()
bp = Blueprint('auth', __name__, url_prefix='/auth')

def init_app(app):
    app.cli.add_command(hash_passwd_command)

@auth.verify_password
def authenticate(username, password):
    user = _fetch_user(username)
    if user is None:
        return False
    if _verify_passwd(password, user["password"]):
        return user
    else:
        return False

@auth.get_user_roles
def get_user_roles(user):
    return [user["role"]]

@click.command('hash-passwd')
@click.argument('passwd')
def hash_passwd_command(passwd: str):
    print(_hash_passwd(passwd.encode('utf8')).hex())

@bp.get('/check')
@auth.login_required
def check_endpoint():
    user = auth.current_user()
    user.pop('password', None)
    return jsonify(user)

def _hash_passwd(passwd):
    salt = os.urandom(32)
    passwd_hash = sha256()
    passwd_hash.update(passwd)
    passwd_hash.update(salt)
    return passwd_hash.digest() + salt

def _fetch_user(username: str):
    db = get_db()
    sql = """
    SELECT id_user, email, password, role FROM users
    WHERE email = %s
    """
    db.execute(sql, (username,))
    return db.fetchone()

def _verify_passwd(provided_passwd, db_passwd):
    passwd_hash = sha256()
    passwd_hash.update(provided_passwd.encode('utf8'))
    expected_hash, salt = _slice_db_passwd(db_passwd)
    passwd_hash.update(salt)
    actual_hash = passwd_hash.digest()
    return expected_hash == actual_hash

def _slice_db_passwd(db_passwd):
    return db_passwd[:-32], db_passwd[-32:]

def _register_user(user: dict):
    db = get_db()
    sql = """
        INSERT INTO users (email, password, role)
        VALUES (%s, %s, 'client')
        """
    email = user["email"]
    password = _hash_passwd(user["password"].encode('utf8'))
    db.execute(sql, (email, password,))

@bp.post("/register")
def register_user_endpoint():
    data = request.get_json()
    _register_user(data)
    return '', 204