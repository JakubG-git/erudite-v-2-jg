DROP TABLE IF EXISTS author CASCADE;
DROP TABLE IF EXISTS book CASCADE;
DROP TABLE IF EXISTS book_author CASCADE;
DROP TABLE IF EXISTS book_tag CASCADE;
DROP TABLE IF EXISTS catalog CASCADE;
DROP TABLE IF EXISTS loan CASCADE;
DROP TABLE IF EXISTS publisher CASCADE;
DROP TABLE IF EXISTS tag CASCADE;
DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS review CASCADE;
DROP TABLE IF EXISTS reservation CASCADE;
DROP TYPE IF EXISTS librole;

CREATE TABLE author (
    id_author SERIAL PRIMARY KEY,
    name character varying(255) NOT NULL
);

CREATE TABLE publisher (
    id_publisher SERIAL PRIMARY KEY,
    name character varying(255) NOT NULL
);

CREATE TABLE book (
    id_book SERIAL PRIMARY KEY,
    title character varying(255) NOT NULL,
    year integer,
    isbn character varying(255) NOT NULL,
    pages integer,
    id_publisher bigint,
    description character varying(4095),
    cover character varying(255),

    CONSTRAINT fk_book_publisher FOREIGN KEY(id_publisher)
    REFERENCES publisher(id_publisher)
);

CREATE TABLE tag (
    id_tag SERIAL PRIMARY KEY,
    color character varying(255) NOT NULL DEFAULT '#b8b894',
    name character varying(255) NOT NULL
);


CREATE TABLE book_author (
    id_book_author SERIAL PRIMARY KEY,
    id_book bigint NOT NULL,
    id_author bigint NOT NULL,

    CONSTRAINT fk_book_author_book FOREIGN KEY(id_book)
    REFERENCES book(id_book),
    CONSTRAINT fk_book_author_author FOREIGN KEY(id_author)
    REFERENCES author(id_author)
);

CREATE TABLE book_tag (
    id_book_tag SERIAL PRIMARY KEY,
    id_book bigint NOT NULL,
    id_tag bigint NOT NULL,

    CONSTRAINT fk_book_tag_book FOREIGN KEY(id_book)
    REFERENCES book(id_book),
    CONSTRAINT fk_book_tag_tag FOREIGN KEY(id_tag)
    REFERENCES tag(id_tag)
);

CREATE TABLE catalog (
    id_catalog SERIAL PRIMARY KEY,
    id_book bigint NOT NULL,
    shelf_mark character varying(255),

    CONSTRAINT fk_catalog_book FOREIGN KEY(id_book)
    REFERENCES book(id_book)
);

CREATE TYPE librole AS ENUM ('client', 'librarian');

CREATE TABLE users (
    id_user SERIAL PRIMARY KEY,
    avatar character varying(255),
    birth_date date,
    description character varying(255),
    email character varying(255) NOT NULL,
    name character varying(255),
    password BYTEA NOT NULL,
    role librole NOT NULL
);

CREATE TABLE loan (
    id_loan SERIAL PRIMARY KEY,
    date_of_loan date NOT NULL,
    date_of_return date NOT NULL,
    prolongations integer DEFAULT 0,
    id_catalog bigint NOT NULL,
    id_user bigint NOT NULL,

    CONSTRAINT fk_loan_catalog FOREIGN KEY (id_catalog)
    REFERENCES catalog(id_catalog),
    CONSTRAINT fk_loan_user FOREIGN KEY (id_user)
    REFERENCES users(id_user)
);


CREATE TABLE reservation (
    id_reservation SERIAL PRIMARY KEY,
    receipt_term date,
    submitted date NOT NULL DEFAULT NOW(),
    id_catalog bigint NOT NULL,
    id_user bigint NOT NULL,

    CONSTRAINT fk_reservation_id_catalog FOREIGN KEY (id_catalog)
    REFERENCES catalog(id_catalog),
    CONSTRAINT fk_reservation_user FOREIGN KEY (id_user)
    REFERENCES users(id_user)
);

CREATE TABLE review (
    id_review SERIAL PRIMARY KEY,
    content character varying(255) NOT NULL,
    id_user bigint NOT NULL,
    id_book bigint NOT NULL,

    CONSTRAINT fk_review_user FOREIGN KEY (id_user)
    REFERENCES users(id_user),
    CONSTRAINT fk_review_book FOREIGN KEY (id_book)
    REFERENCES book(id_book)
);
