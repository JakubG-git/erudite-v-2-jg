# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

"""
Module providing generic request data parsing functionality. All exceptions
raised by this modules' functions are automatically handled app,
so try catch blocks are required to use them.
"""

from erudite.error import NotProvidedException, ParseIdException

def parse_id_from_query(resource: str, args: dict) -> int:
    """
    Given resource name and query args dict (as returned by flasks'
    request.args property), extract resource id by searching for key
    id_{resource} and raise appropriate exceptions.

    Parameters
    ----------
    resource : str
        resource name, pretty much equivalent to its' sql database table
    args : dict
        dictionary containing query parameters, returned by flasks' request.args
        property

    Returns
    -------
    int
        extracted resource id

    Raises
    ------
    NotProvidedException
        Could not find id_{resource} in provided args dict
    ParseIdException
        id_{resource} either maps to non-integer value, or is negative

    Examples
    --------
    >>> from flask import Bluepring, request, jsonify

    ...

    >>> bp.get('/get_for_book')
    >>> def get_for_book():
    >>>     id_book = parse_id_from_query('book', request.args)
    >>>     some_things = fetch_by_book_id(id_book)
    >>>     return jsonify(some_things)

    """

    idx = args.get(f"id_{resource}")
    if idx is None:
        raise NotProvidedException(f"id_{resource}")
    try:
        idx = int(idx)
    except ValueError:
        raise ParseIdException("book")
    if idx <= 0:
        raise ParseIdException("book")
    return idx
