# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

import requests

API_URL = "https://www.googleapis.com/books/v1/"

class APIException(Exception):
    def __init__(self, status_code):
        super().__init__()
        self._status_code = status_code

    def __str__(self):
        return f"Google Books API returned error status code {self._status_code}"

def fetch_book_by_isbn(isbn: str):
    url = API_URL + "volumes?q=isbn:" + isbn
    r = requests.get(url)
    return _process_response(r)

def _process_response(r):
    if r.status_code != 200:
        raise APIException(r.status_code)
    data = r.json()
    if "items" not in data:
        return None

    book = _volume_to_native_schema(data["items"][0])
    return book

def _volume_to_native_schema(volume: dict):
    volume = volume["volumeInfo"]
    book = dict()
    book["title"] = volume["title"]
    book["isbn"] = volume["industryIdentifiers"][1]["identifier"]
    book["authors"] = volume["authors"]
    book["pages"] = volume.get("pageCount")
    book["year"] = int(volume["publishedDate"][:4])
    book["publisher"] = volume.get("publisher")
    book["description"] = volume.setdefault("description", "")
    if "imageLinks" in volume:
        book["cover"] = volume["imageLinks"]["thumbnail"]
    return book
