# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from jsonschema import ValidationError

"""
Module for defining app exceptions and converting them into meaningful
error http responses.
"""

class InvalidRequestException(Exception):
    def __init__(self, msg: str):
        super().__init__()
        self._msg = msg

    def __str__(self):
        return self._msg

class NotProvidedException(InvalidRequestException):
    def __init__(self, var_name: str):
        super().__init__(f"{var_name} not provided")

class ParseException(InvalidRequestException):
    def __init__(self, var_name: str, expected: str):
        super().__init__(f"{var_name} should be {expected}")

class ParseIdException(ParseException):
    def __init__(self, resource: str):
        super().__init__(f"id_{resource}", "a positive integer")

class NotFoundException(Exception):
    def __init__(self, resource: str, idx: int):
        super().__init__()
        self._resource = resource
        self._idx = idx

    def __str__(self):
        return f"Resource {self._resource} with id {self._idx} not found"

def init_error_handling(app):
    @app.errorhandler(InvalidRequestException)
    def invalid_request(e):
        return str(e), 400
    @app.errorhandler(NotFoundException)
    def not_found(e):
        return str(e), 404
    @app.errorhandler(ValidationError)
    def invalid_data(e):
        return str(e), 400
